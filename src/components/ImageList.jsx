import React, { Component } from "react";
import { baseURL } from "../constants";
import ImageCard from "./ImageCard";
import "../styles/imageList.css";
import Modal from "@material-ui/core/Modal";
import ImageDetail from "./ImageDetail";
import PannellumReact from "./Pannellum";

class ImageList extends Component {
  constructor(props) {
    super(props);
    this.state = { currentImage360: "", modalOpen: false, currentImage: {} };
    this.PannellumRef = React.createRef();
  }
  onExpandClick = (image) => {
    this.setState({
      currentImage360: image.image360,
      modalOpen: true,
      currentImage: image,
    });
  };
  onPannellumLoad = () => {
    this.PannellumRef.current.getViewer().toggleFullscreen();
  };
  handleModalClose = () => {
    this.setState({ modalOpen: false });
  };
  render() {
    const images = this.props.images;
    const imageModal = (
      <Modal open={this.state.modalOpen} onClose={this.handleModalClose}>
        <div className="modal-container">
          <div className="image-modal">
            <i
              onClick={this.handleModalClose}
              className="icon large window close  icon-size"
            ></i>
            <PannellumReact
              onLoad={this.onPannellumLoad}
              PannellumRef={this.PannellumRef}
              image360={baseURL + this.state.currentImage360}
            />
          </div>
        </div>
      </Modal>
    );

    return (
      <div>
        <ImageDetail image={this.state.currentImage} />
        {imageModal}
        <div className="image-list-container">
          {images.map((image) => {
            return (
              <ImageCard
                key={image.id}
                onExpandClick={this.onExpandClick}
                image={image}
              />
            );
          })}
        </div>
      </div>
    );
  }
}

export default ImageList;
