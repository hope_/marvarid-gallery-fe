import React, { Component } from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import "../styles/category.css";
import Category from "./Category";
import { PrevArrow, NextArrow } from "./SlickArrows";
class CategoryList extends Component {
  constructor(props) {
    super(props);
    this.state = { imageList: [] };
  }
  onCategoryClick = async (category) => {
    this.props.onCategoryClick(category);
  };

  render() {
    const categories = this.props.categories;

    const settings = {
      className: "center",
      focusOnSelect: false,
      speed: 300,
      slidesToShow: 6,
      slidesToScroll: 1,
      nextArrow: <NextArrow />,
      prevArrow: <PrevArrow />,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            initialSlide: 2,
          },
        },
      ],
    };
    const slides = categories.map((category) => (
      <Category
        onCategoryClick={this.onCategoryClick}
        key={category.id}
        category={category}
        activeCategoryId={this.props.activeCategoryId}
      />
    ));

    return (
      <div className="category-list-wrapper">
        <div className="slider-container">
          {categories !== null && categories.length > 0 && (
            <Slider {...settings}>{slides}</Slider>
          )}
        </div>
      </div>
    );
  }
}

export default CategoryList;
