import React from "react";
import { baseURL } from "../constants";
import "../styles/imageCard.css";

const ImageCard = ({ image, onExpandClick }) => {
  return (
    <div className="image-card" onClick={() => onExpandClick(image)}>
      <img src={baseURL + image.image} alt={image.description} />
      <p>{image.description}</p>
    </div>
  );
};
export default ImageCard;
