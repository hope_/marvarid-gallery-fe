import React from "react";
import { baseURL } from "../constants";
import "../styles/category.css";

const Category = ({ category, onCategoryClick, activeCategoryId }) => {
  return (
    <div
      id={category.id}
      className={`category-container${
        category.id === activeCategoryId ? " active" : ""
      }`}
      onClick={() => onCategoryClick(category)}
    >
      <img src={baseURL + category.icon} alt={category.name} />
      <span>{category.name}</span>
    </div>
  );
};
export default Category;
