import React, { Component } from "react";
import "../styles/searchBar.css";
class Searchbar extends Component {
  state = {};
  onFormSubmit = (event) => {
    event.preventDefault();
    this.props.onTermSubmit();
  };
  onInputChange = (term) => {
    this.props.onInputChange(term);
  };
  render() {
    return (
      <div className="searchbar-container">
        <form onSubmit={this.onFormSubmit}>
          <div className="field">
            <label htmlFor="image-search">Image Search</label>
            <div className="input-button">
              <input
                id="image-search"
                type="text"
                value={this.props.term}
                onChange={(e) => this.onInputChange(e.target.value)}
              />
              <button type="submit">
                <i className="icon search large"></i>
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default Searchbar;
