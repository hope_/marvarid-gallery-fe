import React from "react";
import "../styles/arrowStyle.css";

export function PrevArrow(props) {
  const { onClick } = props;
  return (
    <div id="prevArrow" onClick={onClick}>
      <i className="arrow circle left icon large"></i>
    </div>
  );
}
export function NextArrow(props) {
  const { onClick } = props;
  return (
    <div id="nextArrow" onClick={onClick}>
      <i className="arrow circle right icon large"></i>
    </div>
  );
}
