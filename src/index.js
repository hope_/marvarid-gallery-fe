import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import App from "./components/App";
import { baseURL } from "./constants";

axios.defaults.baseURL = baseURL;
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
