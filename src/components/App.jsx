import React, { Component } from "react";
import axios from "axios";
import CategoryList from "./CategoryList";
import ImageList from "./ImageList";
import "../styles/app.css";
import SearchBar from "./SearchBar";
class App extends Component {
  state = {
    categories: [],
    currentImageList: [],
    activeCategoryId: 1,
    term: "",
  };
  async componentDidMount() {
    const response = await axios.get("/images/categories/");
    const defaultImageList = await axios.get(
      `/images/${response.data[0].slug}/all/`
    );
    this.setState({
      categories: response.data,
      currentImageList: defaultImageList.data,
    });
  }
  onCategoryClick = async (category) => {
    this.setState({ activeCategoryId: category.id, term: "" });
    const response = await axios.get(`/images/${category.slug}/all/`);
    this.setState({ currentImageList: response.data });
  };
  onInputChange = (term) => {
    this.setState({ term });
  };
  onTermSubmit = async () => {
    const response = await axios.get("/images/", {
      params: {
        term: this.state.term,
      },
    });
    this.setState({ currentImageList: response.data, activeCategoryId: null });
  };
  render() {
    return (
      <div className="app-container">
        <SearchBar
          onTermSubmit={this.onTermSubmit}
          term={this.state.term}
          onInputChange={this.onInputChange}
        />
        <CategoryList
          onCategoryClick={this.onCategoryClick}
          categories={this.state.categories}
          activeCategoryId={this.state.activeCategoryId}
        />
        <ImageList images={this.state.currentImageList} />
      </div>
    );
  }
}

export default App;
