import React from "react";
import { Pannellum } from "pannellum-react";

const PannellumReact = ({ image360, PannellumRef, onLoad }) => {
  return (
    <Pannellum
      ref={PannellumRef}
      width="100%"
      height="100%"
      image={image360}
      autoLoad={true}
      onLoad={() => onLoad()}
      showZoomCtrl={true}
    ></Pannellum>
  );
};

export default PannellumReact;
